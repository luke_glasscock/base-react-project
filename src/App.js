import "./styles.css";

export default function App() {
  return (
    <div className="App">
      <h1>Base React App</h1>
      <h2>Edit your text here!</h2>
    </div>
  );
}
